/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { Component} from 'react'
import {
    StatusBar,
    StyleSheet,
    View,
    TouchableOpacity,
    Text,
} from 'react-native'
import { Switch, Route, Redirect } from 'react-router'
import { NativeRouter, Link } from 'react-router-native'
import {Navigation, Card, Tabs, Tab, BottomNavigation} from 'react-router-navigation'
import React from "react";

const PRIMARY_COLOR = 'rgb(226, 68, 68)'
const SECONDARY_COLOR = 'rgb(226, 144, 68)'

const styles = StyleSheet.create({
    scene: {
        flex: 1,
        padding: 18,
    },
    tabs: {
        backgroundColor: PRIMARY_COLOR,
    },
    tab: {
        paddingTop: 10,
        opacity: 10,
    },
    indicator: {
        backgroundColor: 'white',
    },
    button: {
        alignSelf: 'flex-start',
        marginTop: 10,
        marginLeft: -8,
        paddingVertical: 10,
        paddingHorizontal: 18,
        borderWidth: 1,
        borderColor: 'grey',
        borderRadius: 3,
    },
    strong: {
        fontWeight: '700',
        marginBottom: 10,
    },
})
type Props = {};
export default class App extends Component<Props> {


    constructor(){
        super();
        this.state = {
            navigation: {},
            card: {},
        }
    }



    renderFistCard = () => {
        return (
            <View style={styles.scene}>
                <Link component={TouchableOpacity} to="/newpage" style={styles.button}>
                    <Text>Push a new scene</Text>
                </Link>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() =>
                        this.setState({
                            navigation: {
                                navBarStyle: {
                                    backgroundColor: PRIMARY_COLOR,
                                    borderBottomWidth: 0,
                                },
                                titleStyle: { color: 'white' },
                                barStyle: 'light-content',
                                backButtonTintColor: 'white',
                            },
                        })
                    }
                >
                    <Text>Change navbar style</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderSecondCard = () => {
        return (
            <View style={styles.scene}>
                <Link component={TouchableOpacity} style={styles.button} to="/tabs">
                    <Text>Push tabs</Text>
                </Link>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => {
                        this.setState(prevState => ({
                            navigation: {
                                ...prevState.navigation,
                                barStyle: 'light-content',
                            },
                            card: {
                                ...prevState.card,
                                navBarStyle: {
                                    backgroundColor: SECONDARY_COLOR,
                                    borderBottomWidth: 0,
                                },
                                titleStyle: { color: 'white' },
                                backButtonTintColor: 'white',
                            },
                        }))
                    }}
                >
                    <Text>Change navbar style</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => {
                        this.setState(prevState => ({
                            card: {
                                ...prevState.card,
                                title: 'New title !',
                            },
                        }))
                    }}
                >
                    <Text>Change title</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderThirdCard = contextRouter => {
        const { location, match } = contextRouter
        return (
            <Switch location={location}>
                <Route
                    exact
                    path={match.url}
                    render={() => <Redirect to={`${match.url}/one`} />}
                />
                <Route
                    render={() => (
                        <Tabs
                            style={styles.container}
                            tabBarStyle={styles.tabs}
                            tabBarIndicatorStyle={styles.indicator}
                        >
                            <Tab
                                path={`${match.url}/one`}
                                label="One"
                                tabStyle={styles.tab}
                                render={this.renderFirstTab}
                            />
                            <Tab
                                path={`${match.url}/two`}
                                label="Two"
                                tabStyle={styles.tab}
                                render={this.renderSecondTab}
                            />
                            <Tab
                                path={`${match.url}/three`}
                                label="Three"
                                tabStyle={styles.tab}
                                render={this.renderThirdTab}
                            />
                        </Tabs>
                    )}
                />
            </Switch>
        )
    }

    renderFourCard = contextRouter => {
        const {location, match} = contextRouter
        return (
            <Switch location={location}>
                <Route
                    exact
                    path={match.url}
                    render={() => <Redirect to={`${match.url}/one`}/>}
                />
                <Route
                    render={() => (
                        <BottomNavigation
                            lazy={false}
                            tabActiveTintColor={"blue"}
                            tabStyle={{opacity: 1}}
                        >
                            <Tab
                                path={`${match.url}/one`}
                                label="One"
                                tabStyle={styles.tab}
                                render={this.renderFirstTab}
                            />
                            <Tab
                                path={`${match.url}/two`}
                                label="Two"
                                tabStyle={styles.tab}
                                render={this.renderSecondTab}
                            />
                            <Tab
                                path={`${match.url}/three`}
                                label="Three"
                                tabStyle={styles.tab}
                                render={this.renderThirdTab}
                            />
                        </BottomNavigation>
                    )}
                />
            </Switch>
        )
    }

    renderFirstTab = contextRouter => {
        const { match } = contextRouter
        const basePath = match && match.url.slice(0, match.url.lastIndexOf('/'))
        return (
            <View style={styles.scene}>
                <Text style={styles.strong}>One</Text>
                <Link replace={true} component={TouchableOpacity} style={styles.button} to="/bottomNavigation">
                    <Text>Push bottom Navigation</Text>
                </Link>
                <Link
                    component={TouchableOpacity}
                    style={styles.button}
                    replace={true}
                    to={`${basePath}/two`}
                >
                    <Text>Go to "two"</Text>
                </Link>
                <Link
                    component={TouchableOpacity}
                    style={styles.button}
                    replace={true}
                    to={`${basePath}/three`}
                >
                    <Text>Go to "three"</Text>
                </Link>
            </View>
        )
    }

    renderSecondTab = contextRouter => {
        const { match } = contextRouter
        const basePath = match && match.url.slice(0, match.url.lastIndexOf('/'))
        return (
            <View style={styles.scene}>
                <Text style={styles.strong}>Two</Text>
                <Link replace={true} component={TouchableOpacity} style={styles.button} to="/bottomNavigation">
                    <Text>Push bottom Navigation</Text>
                </Link>
                <Link
                    component={TouchableOpacity}
                    style={styles.button}
                    replace={true}
                    to={`${basePath}/one`}
                >
                    <Text>Go to "one"</Text>
                </Link>
                <Link
                    component={TouchableOpacity}
                    style={styles.button}
                    replace={true}
                    to={`${basePath}/three`}
                >
                    <Text>Go to "three"</Text>
                </Link>
            </View>
        )
    }

    renderThirdTab = contextRouter => {
        const { match } = contextRouter
        const basePath = match && match.url.slice(0, match.url.lastIndexOf('/'))
        return (
            <View style={styles.scene}>
                <Text style={styles.strong}>Three</Text>
                <Link replace={true} component={TouchableOpacity} style={styles.button} to="/bottomNavigation">
                    <Text>Push bottom Navigation</Text>
                </Link>
                <Link
                    component={TouchableOpacity}
                    style={styles.button}
                    replace={true}
                    to={`${basePath}/one`}
                >
                    <Text>Go to "one"</Text>
                </Link>
                <Link
                    component={TouchableOpacity}
                    style={styles.button}
                    // replace={true}
                    to={`${basePath}/two`}
                >
                    <Text>Go to "two"</Text>
                </Link>
            </View>
        )
    }

    render() {
        const { navigation, card } = this.state
        return (
            <NativeRouter>
                <React.Fragment>
                    <StatusBar barStyle={navigation.barStyle} />
                    <Navigation
                        navBarStyle={navigation.navBarStyle}
                        titleStyle={navigation.titleStyle}
                        backButtonTintColor={navigation.backButtonTintColor}

                    >
                        <Card exact path="/" title="BottomNavigation" render={this.renderFourCard} />

                    </Navigation>
                </React.Fragment>
            </NativeRouter>
        )
    }

//
//
// <Card exact path="/" title="Index" render={this.renderFistCard} />
// <Card
//     titleStyle={card.titleStyle || navigation.titleStyle}
// navBarStyle={card.navBarStyle || navigation.navBarStyle}
// backButtonTintColor={
//     card.backButtonTintColor || navigation.backButtonTintColor
// }
// path="/newpage"
// render={this.renderSecondCard}
// title={card.title || 'New scene'}
// />
// <Card path="/tabs" title="Tabs" render={this.renderThirdCard} />



    // render() {
    //     return (
    //         <NativeRouter>
    //             <View style={styles.container}>
    //                     <Route
    //                         exact path="/app"
    //                         render={({match: {url}}) => <Redirect to={`${url}/home`}/>}
    //                     />
    //                     <Route
    //                         path="/app"
    //                         render={({match: {url}}) => (
    //                             <BottomNavigation
    //                                 lazy={false}
    //                                 tabActiveTintColor={"blue"}
    //                                 tabStyle={{opacity: 1}}
    //                             >
    //                                 <Tab
    //                                     path={`${url}/home`}
    //                                     // component={Home}
    //                                     render={() => (
    //                                         <Text style={styles.welcome}> Home Screen!</Text>
    //                                     )}
    //                                     label="Home"
    //                                     labelStyle={styles.label}
    //                                     // renderTabIcon={({ focused }) => getIcon('home', 22, focused)}
    //                                 />
    //                                 <Tab
    //                                     path={`${url}/search`}
    //                                     // component={Search}
    //                                     render={() => (
    //                                         <Text style={styles.welcome}> Search Screen!</Text>
    //                                     )}
    //                                     label="Search"
    //                                     labelStyle={styles.label}
    //                                     // renderTabIcon={({ focused }) => getIcon('search', 22, focused)}
    //                                 />
    //                                 <Tab
    //                                     path={`${url}/profile`}
    //                                     render={() => (
    //                                         <Text style={styles.welcome}> Profile Screen!</Text>
    //                                     )}
    //                                     label="Profile"
    //                                     labelStyle={styles.label}
    //                                     // renderTabIcon={({ focused }) => getIcon('profile', 22, focused)}
    //                                 />
    //                             </BottomNavigation>
    //                         )}
    //                     />
    //             </View>
    //         </NativeRouter>
    //     );
    // }
}


/// TODO runnable navigation!!!
// export default class App extends Component<Props> {
//     render() {
//         return (
//             <NativeRouter>
//                 <Navigation>
//                     <Card
//                         exact path="/"
//                         render={() => (
//                             <Link to="/hello">
//                                 <Text style={styles.welcome}> Press it!</Text>
//                             </Link>
//                         )}
//                     />
//                     <Card
//                         path="/hello"
//                         render={
//                             () => <Text style={styles.welcome}>Hello </Text>
//                         }
//                     />
//                 </Navigation>
//             </NativeRouter>
//         );
//     }
// }


// TODO first app example
// export default class App extends Component<Props> {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Text style={styles.welcome}>Welcome to React Native!</Text>
//         <Text style={styles.instructions}>To get started, edit App.js</Text>
//         <Text style={styles.instructions}>{instructions}</Text>
//       </View>
//     );
//   }
// }
//
// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center',
//         backgroundColor: '#F5FCFF',
//     },
//     welcome: {
//         fontSize: 20,
//         textAlign: 'center',
//         margin: 10,
//     },
//     instructions: {
//         textAlign: 'center',
//         color: '#333333',
//         marginBottom: 5,
//     },
// });
